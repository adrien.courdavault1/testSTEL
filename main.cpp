#include <iostream>
#include <array>
#include <bitset>
#include <cassert>

///Notes
/// I assumed the original number sequence was random and the point was in the memory structure and the code.
/// if there was a mathematical generator to find for this sequence i could look for it
/// 
/// I did not make the structures thread safe (because it was useless)
///
/// the no dynamic allocation constrint is checked by the fact I create only 2 structures statically:
/// - the original sequence
/// - the Pool of nodes (used for trees and lists)
///
/// The tree structure choosen is a very simplistic one which actually wastes space considering that only leafs have values
/// 
/// if my goal was memory optimization, I would only use a simplistic array, the "tree structure" in itself could just be deduced from 
/// the way we traverse a contiguous memory sequence.
///
/// I used siostream, array, bitset as conveniences,
/// array and bitset both have excellent performance with no dynamic allocation and have bound checking
/// i could have used a C array, and create my own bitset class and check bounds but that seemed redondant



/// With this structure its easy to add children,
/// although here the example is binary and balanced, this would work for other examples
/// When using this for a list, i use the first pointer as a previous pointer
/// When using this for a tree i use it as a child pointer

///tree structure adapted for easily adding branched (while binary tree is usually left/right)
///     root
///    /
///   child     ->       next       ->     next
///  /                  /                 /
/// child -> next     child -> next     child -> next -> next
template <typename ValueT>
struct TreeOrListNode {
	TreeOrListNode *childOrPrevious = nullptr;
	TreeOrListNode *next = nullptr;
	ValueT value;
};

/// for print
template <typename ValueT>
std::ostream &operator<<(std::ostream &os, TreeOrListNode<ValueT> const &n) {
	os << "val: " << value << "; ptrs: [" << m.childOrPrevious << "; " << m.next << "]" << std::endl;
	if (m.childOrPrevious) {
		std::cout << "child: " << m.childOrPrevious << std::endl;
	}
	if (m.next) {
		std::cout << "next: " << m.childOrPrevious << std::endl;
	}
}

template <typename ValueT, size_t PoolSize>
class NodePool {
private:
	std::array<TreeOrListNode<ValueT>, PoolSize> mThePool;
	std::bitset<PoolSize> mUsed;

	//this could be removed to save 4 bytes, but reduces the time we search for the next free node
	size_t mIndex = 0;
public:
	TreeOrListNode<ValueT> * getNode() {
		for (auto i = mIndex; i < (PoolSize + mIndex); i++) {
			auto j = i % PoolSize;
			auto used = mUsed[j];
			if (!used) {
				mUsed.set(j);
				auto node = &(mThePool[j]);
				node->value = -1;
				node->next = nullptr;
				node->childOrPrevious = nullptr;
				return node;
			}
		}
		return nullptr;
	}

	///call with the address of the ode you release
	///this does no allocation, just marks it free in the pool
	void releaseNode(TreeOrListNode<ValueT> * tn) {
		auto j = (tn - &(mThePool[0]));
		mUsed.reset(j);
	}
};

////////////////////////////////////////////
/// THE MEMORY is HERE
/// allocation of all the memory in the program for trees and lists in a pool
/// list and tree use the same Node so only 1 pool
NodePool<int, 256> theUniqueNodePool;
/// and original sequence in static array
const std::array<int, 16> originalSequence = { 5, 15, 9, 81, 23, 37, 45, 8, 11, 16, 12, 63, 7, 71, 99, 2 };



/// Works only for a binary tree with only leafs values
/// 


void populate(TreeOrListNode<int>* localRoot, int localRootDepth, int maxDepth, int sequencePosition) {
	localRoot->childOrPrevious = theUniqueNodePool.getNode();
	localRoot->childOrPrevious->next = theUniqueNodePool.getNode();
	auto localDepth = localRootDepth + 1;
	std::cout << "populate " << localDepth << ", " << maxDepth << ", " << sequencePosition << std::endl;

	if (maxDepth <= localDepth) {
		auto v1 = originalSequence[sequencePosition];
		auto v2 = originalSequence[sequencePosition + 1];
		std::cout << "adding values at the bottom of the tree " << v1 << ", " << v2 << std::endl;

		localRoot->childOrPrevious->value = v1;
		localRoot->childOrPrevious->next->value = v2;
	}
	else {
		populate(localRoot->childOrPrevious, localDepth, maxDepth, sequencePosition);
		populate(localRoot->childOrPrevious->next, localDepth, maxDepth, sequencePosition + (1 << (maxDepth - localDepth)));
	}
}

TreeOrListNode<int>* createBinaryTreeWithOnlyLeafs() {
	//depth of tree
	int depth = 0;
	for (int d = originalSequence.size(); d > 0; d>>=1){
		if (d & 1 && d != 1) {
			std::cout << "originalSequence.size() is not a pow of 2: " << originalSequence.size();
			assert(-1);
		}
		else if (d == 1) {
			break;
		}
		depth++;
	}
	std::cout << "found depth of tree is " << depth << std::endl;
	
	auto root = theUniqueNodePool.getNode();
	populate(root, 0, depth, 0);

	return root;
}

void printDoubleLinkedListFwdBwd(TreeOrListNode<int>* depthFirstList) {
	auto cur = depthFirstList->next; // depthFirstList ptr is just a pointer to the list
	std::cout << "print forward" << std::endl;
	while (cur->next != nullptr) {
		std::cout << cur->value << ", ";
		cur = cur->next;
	}
	std::cout << "print backward" << std::endl;

	while (cur->childOrPrevious != nullptr) {
		std::cout << cur->value << ", ";
		cur = cur->childOrPrevious;
	}
}

TreeOrListNode<int>* createDoubleLinkedListDepthFirst(TreeOrListNode<int>* tree, TreeOrListNode<int>* topList) {

	if (tree->childOrPrevious == nullptr) {
		//we reached the bottom
		auto list = topList;
		list->next = theUniqueNodePool.getNode();
		//std::cout << "insert value " << tree->value << std::endl;
		list->next->childOrPrevious = topList;
		list->next->value = tree->value;
		list = list->next;
		return list;
	}
	else {
		auto endOfList = topList;
		for (auto child = tree->childOrPrevious; child != nullptr; child = child->next) {
			//std::cout << "dig" << std::endl;
			endOfList = createDoubleLinkedListDepthFirst(child, endOfList);
		}
		return endOfList;
		
	}
}


int main (int argc, char ** argv){
	std::cout << "Print Original leaf sequence:" << std::endl;
	for (auto e : originalSequence) {
		std::cout << e << " ";
	}
	std::cout << std::endl;
	auto tree = createBinaryTreeWithOnlyLeafs();	
	auto depthFirstList = theUniqueNodePool.getNode();

	createDoubleLinkedListDepthFirst(tree, depthFirstList);
	printDoubleLinkedListFwdBwd(depthFirstList);
	std::cout << "the end" << std::endl;
    return 0;
}